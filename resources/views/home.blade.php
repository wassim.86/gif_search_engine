@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
                <hr />
                <div class="card-body">
                <div class="search">
                    <div class="icon"></div>
                        <form id="searchForm" action = "{{route('search')}}" method="post">
                        @CSRF
                            <div class="input">
                                <input type="text" name="search" placeholder="Search" id="search">
                                <input type="submit" class="submit" value="Submit" >
                            </div>
                        </form>
                        <span class="clear" onclick="document.getElementById('search').value = ''"></span>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="card-body">
                <table class="table table-bordered" id = "table">
                  <tbody>

                  </tbody>
                </table>
              </div>
<!-- jQuery -->
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script>
const icon = document.querySelector('.icon');
const search = document.querySelector('.search');
icon.onclick = function (){
    search.classList.toggle('active');
}
$.ajax({
    url: '{{Illuminate\Support\Facades\Session::get("url")}}',
    method: "GET",
    Accept: 'application/json',
        success: function (response) {
            changePage(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error");
        }
});
function changePage(response)
{

    for (var i = 0 ; i < response.data.length ; i++) {
        var sss = "<tr><td>"+(i+1)+"</td>"
                        +"<td>"+response.data[i].title+"</td>"
                        +"<td>"
                        +"<div class='attachment-block clearfix' style='width: fit-content;height: fit-content;'>"
                            +"<a href='/show/"+i+"'>"
                                +"<img src=" + response.data[i].images.original_still.url +" class='elevation-3 attachment-img' alt='Service image'/>"
                            +"</a></div></td></tr>";
        $("table").append(sss);
    }
}
</script>
@endsection
<style>
img
{
    max-width: 20%;
    max-hight: 20%;
}
</style>
