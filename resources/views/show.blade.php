@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-tools">
                  <a href="{{route('home')}}" class="btn btn-sm btn-danger">Back</a>
                </div>
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered" id = "table">
                        <tbody>

                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>
@endsection
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script>
const i = {{$i}};
$.ajax({
    url: '{{Illuminate\Support\Facades\Session::get("url")}}',
    method: "GET",
    Accept: 'application/json',
        success: function (response) {
            changePage(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("error");
        }
});
function changePage(response)
{console.log(response);
        var sss ="<div class='attachment-block clearfix' style='width: fit-content;height: fit-content;'>"
                            +"<a href='/show/"+i+"'>"
                                +"<img style='padding: 13%;' src=" + response.data[i].images.downsized_large.url +" class='elevation-3 attachment-img' alt='Service image'/>"
                            +"</a></div>";
        $("table").append(sss);
}
</script>
