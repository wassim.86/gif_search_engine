<?php

return[
    "button"=>[
        "create"=>"Create",
        "cancel"=>"Cancel",
        "browse"=>"Browse",
        "update"=>"Save changes",
    ],
    "label"=>[
        "user"=>[
            "edit_title"=>"Edit profile",
            "image"=>"Image",
            "first_name"=>"First name",
            "last_name"=>"Last name",
            "email"=>"Email address",
            "password"=>"Password",
            "placeholder"=>[
                "image"=>"Choose image",
                "first_name"=>"Enter first name",
                "last_name"=>"Enter last name",
                "email"=>"Enter email address",
                "password"=>"Enter password",
            ],
        ],
    ],
];
